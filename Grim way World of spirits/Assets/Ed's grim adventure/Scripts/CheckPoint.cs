﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CheckPoint : MonoBehaviour
{
    private GameObject enemy;

    private void Awake()
    {
        enemy = GameObject.Find("Enemy");
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.tag == "Player")
        {
            var healthSystem = collision.gameObject.GetComponent<HealthSystem>();
            var creatures = new List<SaveCreature>();
            creatures.Add(new SaveCreature(gameObject.name, collision.transform.position, healthSystem.CurrentHealth, healthSystem.MaxHealth));
            List<GameObject> enemies = new List<GameObject>();
            for (int i = 0; i < enemy.transform.childCount; i++)
                enemies.Add(enemy.transform.GetChild(i).gameObject);
            for (int i = 0; i < enemies.Count; i++)
            {
                var healthEnemy = enemies[i].GetComponent<HealthSystem>();
                creatures.Add(new SaveCreature(enemies[i].name, enemies[i].transform.position, healthEnemy.CurrentHealth, healthEnemy.MaxHealth));
            }
            var save = new Save(SceneManager.GetActiveScene().name, creatures);
            save.Write($"{Application.persistentDataPath}\\Save.json");
        }
    }
}
