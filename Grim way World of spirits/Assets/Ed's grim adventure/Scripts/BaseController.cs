﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//! \brief Базовый контроллер
//! \author Заплетин Дмитрий Евгеньевич
//! \version 1.0
//! \date 17.11.2019
//! \warning Только для наследования контроллеров
[RequireComponent(typeof(Rigidbody2D), typeof(Animator))]
public class BaseController : MonoBehaviour
{
    //! Скорость передвижения
    [SerializeField]
    protected float moveSpeed;
    //! Скорость передвижения
    public float MoveSpeed { get => moveSpeed; protected set => moveSpeed = value; }
    //! Игрок перемещается?
    protected bool IsMoving { get; set; }
    //! Атакует?
    public bool IsAttack { get; protected set; }
    //! Мёртв?
    public bool IsDied { get; protected set; }
    //! Ссылка на аниматор
    protected Animator Animator { get; set; }
    //! Ссылка на физическое тело
    protected Rigidbody2D RigidBody { get; set; }
    //! Ссылка на цель атаки
    protected GameObject Target { get; set; }
    //! Цель обнаружена?
    public bool IsReachableTarget { get; protected set; }
    //! Тег цели
    public string TargetTag { get; protected set; }
    //! Направление по горизонтали
    protected float Horizontal { get; set; }
    //! Направление по вертикали
    protected float Vertical { get; set; }
    //! Вектор атаки
    protected Vector2 AttackVector { get; set; }

    protected virtual void Start()
    {
        Animator = GetComponent<Animator>();
        RigidBody = GetComponent<Rigidbody2D>();
    }

    protected virtual void LateUpdate()
    {
        Attack(false);
    }
    //! Движение
    protected virtual void Move()
    {
        Move_DefinitionTarget();
        Move_Movement();
    }
    //! Определение направления движения
    protected virtual void Move_DefinitionTarget()
    {
        Horizontal = Mathf.Round(Random.Range(-1f, 1f));
        Vertical = Mathf.Round(Random.Range(-1f, 1f));
    }
    //! Перемещение
    protected virtual void Move_Movement()
    {
        RigidBody.velocity = new Vector2(Horizontal * moveSpeed, Vertical * moveSpeed);
    }
    //! Анимирование движения
    protected virtual void MoveAnimate()
    {
        Animator.SetFloat("MoveX", Horizontal);
        Animator.SetFloat("MoveY", Vertical);
    }
    protected virtual void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == TargetTag)
            IsReachableTarget = true;
        DefinitionTarget(collision);
    }

    protected virtual void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.gameObject.tag == TargetTag)
            IsReachableTarget = true;
        DefinitionTarget(collision);
    }

    protected virtual void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.tag == TargetTag)
            IsReachableTarget = false;
        DefinitionTarget(collision);
    }
    //! Определение цели
    //! \param[in] collision - Определённый коллайдер
    //! \param[in] isReachableTarget - Цель определена?
    protected virtual void DefinitionTarget(Collider2D collision)
    {
        if (collision.gameObject.tag == TargetTag)
        {
            if (IsReachableTarget)
                Target = collision.gameObject;
            else
                Target = null;
        }
    }
    //! Воспроизведение атаки
    //! \param[in] isAttack - Атакует?
    protected virtual void Attack(bool isAttack)
    {
        if (Target == null)
        {
            IsAttack = false;
            Animator.SetBool("IsAttack", IsAttack);
        }
        else if (IsReachableTarget)
        {
            IsAttack = isAttack;
            AttackVector = (Target.transform.position - gameObject.transform.position).normalized;
            Animator.SetBool("IsAttack", isAttack);
            Animator.SetFloat("AttackX", Mathf.Round(AttackVector.x));
            Animator.SetFloat("AttackY", Mathf.Round(AttackVector.y));
        }
    }
    //! Проигрывание смерти
    //! \param[in] timerDied - Время смерти
    public virtual void Died(float timeDied)
    {
        var colliders = gameObject.GetComponents<BoxCollider2D>();
        MoveSpeed = 0;
        for (int i = 0; i < colliders.Length; i++)
            colliders[i].enabled = false;
        Animator.SetBool("IsDied", true);
        IsDied = true;
    }
}
