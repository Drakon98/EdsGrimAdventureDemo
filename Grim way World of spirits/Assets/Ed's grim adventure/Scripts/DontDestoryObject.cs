﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//! \brief Класс для переноса объектов с одного игрового уровня на другой (пока только для ГГ)
//! \author Заплетин Дмитрий Евгеньевич
//! \version 1.0
//! \date 03.11.2019
//! \warning Для снятия скрипта есть функция в классе UIManager: public void LoadScene(string nameScene, bool isClear)
public class DontDestoryObject : MonoBehaviour
{
    //! Делает объект неразрушимым при загрузке новой сцены
    private void Awake()
    {
        DontDestroyOnLoad(gameObject);
    }
}
