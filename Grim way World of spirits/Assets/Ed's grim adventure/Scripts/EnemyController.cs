﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//! \brief Контроллер врага
//! \author Заплетин Дмитрий Евгеньевич
//! \version 3.0
//! \date 17.11.2019
public class EnemyController : BaseController
{
    //! Время остановки
    [SerializeField]
    private float timeToStop;
    //! Время остановки
    public float TimeToStop { get => timeToStop; private set => timeToStop = value; }
    //! Таймер остановки
    private float timerToStop;
    //! Таймер остановки
    public float TimerToStop { get => timerToStop; private set { if (value <= 0f) { timerToStop = timeToStop; IsMoving = true; } else timerToStop = value; } }
    //! Время движения
    [SerializeField]
    private float timeToMove;
    public float TimeToMove { get => timeToMove; private set => timeToMove = value; }
    //! Таймер движения
    private float timerToMove;
    //! Таймер движения
    public float TimerToMove { get => timerToMove; private set { if (value <= 0f) { timerToMove = timeToMove; IsMoving = false; } else timerToMove = value; } }

    protected override void Start()
    {
        base.Start();
        Animator.SetFloat("MoveY", -1);
        TargetTag = "Player";
        TimerToStop = TimeToStop;
        TimerToMove = TimeToMove;
    }

    void Update()
    {
        Attack(IsReachableTarget);
        if (IsMoving)
        {
            if (TimerToMove == TimeToMove && !IsAttack && !IsDied)
            {
                Move();
            }
        }
        MoveUpdate();
    }
    //! Движение, если время остановки вышло
    protected override void Move()
    {
        base.Move();
        MoveAnimate();
    }
    //! Обновление таймеров движения
    private void MoveUpdate()
    {
        Animator.SetBool("IsMove", IsMoving);
        if (IsMoving)
        {
            TimerToMove -= Time.deltaTime;
        }
        else
        {
            TimerToStop -= Time.deltaTime;
            RigidBody.velocity = Vector2.zero;
        }
    }
    //! Проигрывание смерти
    //! \param[in] timeDied - время смерти
    public override void Died(float timeDied)
    {
        base.Died(timeDied);
        Destroy(gameObject, timeDied);
    }
}
