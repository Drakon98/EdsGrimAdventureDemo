﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class SettingsObserver : MonoBehaviour
{
    [SerializeField]
    private Settings _settings;

    [SerializeField]
    private GameSettings _musicSettings;

    [SerializeField]
    private GameSettings _soundSettings;

    private string _path;

    private void Start()
    {
        _path = $"{Application.persistentDataPath}\\settings.json";
   
        if (!File.Exists(_path))
            _settings.Write(_path);
            _settings.Read(_path);
            _musicSettings?.LoadSettings(_settings.Music);
            _soundSettings?.LoadSettings(_settings.Sound);
    }

    public void ChangeMusic()
    {
        _settings.Music = _musicSettings.music.volume;
        _settings.Write(_path);
        _musicSettings.LoadSettings(_settings.Music);
    }

    public void ChangeSound()
    {
        _settings.Sound = _soundSettings.music.volume;
        _settings.Write(_path);
        _soundSettings.LoadSettings(_settings.Sound);
    }
}
