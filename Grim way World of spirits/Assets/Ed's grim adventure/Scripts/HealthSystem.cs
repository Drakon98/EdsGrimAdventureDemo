﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
//! \brief Система восстановления здоровья
//! \author Заплетин Дмитрий Евгеньевич
//! \version 1.0
//! \date 03.11.2019
public class HealthSystem : MonoBehaviour
{
    //! Максимальное количество здоровья
    [SerializeField]
    private uint maxHealth;
    //! Максимальное количество здоровья
    public uint MaxHealth { get => maxHealth; private set { if (value == 0) maxHealth = 1; else maxHealth = value; } }
    //! Текущее количество здоровья
    [SerializeField]
    private uint currentHealth;
    //! Текущее количество здоровья
    public uint CurrentHealth { get => currentHealth; private set { if (value > MaxHealth) currentHealth = MaxHealth; else currentHealth = value; } }
    //! Существо мертво?
    public bool IsDied;
    //! События возникающие при смерти
    [SerializeField]
    private UnityEvent eventIsDied;
    //! События возникающие при смерти
    public UnityEvent EventIsDied { get => eventIsDied; private set => eventIsDied = value; }
    //! Ссылка на слайдер здоровья
    [SerializeField]
    private Slider hitPoints;
    //! Ссылка на слайдер здоровья
    public Slider HitPoints { get => hitPoints; private set => hitPoints = value; }
    //! Ссылка на текст здоровья
    [SerializeField]
    private Text healthText;
    //! Ссылка на текст здоровья
    public Text HealthText { get => healthText; private set => healthText = value; }

    private void Start()
    {
        if (UIManager.IsNextLevel)
        {
            VerificationHealth();
            LoadHitPoints();
        }
    }
    //! Повышение здоровья
    //! \param[in] hitPoints - Количество восстанавливаемого здоровья
    public void Healing(uint hitPoints)
    {
        CurrentHealth += hitPoints;
        IsDied = false;
        LoadHitPoints();
    }
    //! Получение урона
    //! \param[in] damage - Количество получаемого урона
    public void SetDamage(uint damage)
    {
        if (damage >= CurrentHealth)
        {
            CurrentHealth = 0;
            PlayDied();
        }
        else
            CurrentHealth -= damage;
        LoadHitPoints();
    }
    //! Увеличение максимального здоровья
    //! \param[in] hitPoints - Количество дополнительного максимального здоровья
    public void GrowthMaxHealth(uint hitPoints)
    {
        MaxHealth += hitPoints;
        LoadHitPoints();
    }
    //! Уменьшение максимального здоровья
    //! \param[in] hitPoints - Количество уменьшаемого максимального здоровья
    public void ReductionMaxHealth(uint hitPoints)
    {
        if (hitPoints > MaxHealth)
            MaxHealth = 1;
        else
            MaxHealth -= hitPoints;
        if (CurrentHealth > MaxHealth)
            CurrentHealth = MaxHealth;
        LoadHitPoints();
    }
    //! Загрузка здоровья
    //! \param[in] maxHealth - максимальное количество здоровья
    //! \param[in] currentHealth - текущее здоровье
    public void LoadHealth(uint maxHealth, uint currentHealth)
    {
        MaxHealth = maxHealth;
        CurrentHealth = currentHealth;
        LoadHitPoints();
        if (CurrentHealth == 0)
            Destroy(gameObject);
    }
    //! Загрузка значений здоровья в слайдер
    private void LoadHitPoints()
    {
        HitPoints.maxValue = MaxHealth;
        if(gameObject.tag == "Player")
            HitPoints.value = MaxHealth - CurrentHealth;
        else
            HitPoints.value = CurrentHealth;
        HealthText.text = $"{CurrentHealth}/{MaxHealth}";
    }
    //! Проверка назначенного здоровья
    private void VerificationHealth()
    {
        if (MaxHealth == 0)
            MaxHealth = 1;
        if (CurrentHealth > MaxHealth)
            CurrentHealth = MaxHealth;
        if (CurrentHealth == 0)
            PlayDied();
    }
    //! Проигрывание смерти
    public void PlayDied()
    {
        IsDied = true;
        if(gameObject.tag != "Player")
            HitPoints.gameObject.SetActive(false);
        EventIsDied.Invoke();
    }
}
