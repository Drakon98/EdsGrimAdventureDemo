﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine. UI;

public class ESC : MonoBehaviour
{
    public GameObject panel;

    private void Start()
    {
 
    }

    void EscPanel()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            panel.SetActive(false);
        }
    }
}
