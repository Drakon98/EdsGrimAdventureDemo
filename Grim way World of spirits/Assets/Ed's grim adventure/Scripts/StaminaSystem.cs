﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//! \brief Система выносливости
//! \author Заплетин Дмитрий Евгеньевич
//! \version 1.0
//! \date 03.11.2019
[RequireComponent(typeof(HealthSystem))]
public class StaminaSystem : MonoBehaviour
{
    //! Выносливость
    [SerializeField]
    private uint stamina;
    //! Выносливость
    public uint Stamina { get => stamina; private set => stamina = value; }
    //! Коэффициент соотношения выносливости здоровью
    [SerializeField]
    private uint healthStaminaRatio;
    //! Коэффициент соотношения выносливости здоровью
    public uint HealthStaminaRatio { get => healthStaminaRatio; private set => healthStaminaRatio = value; }
    //! Ссылка на HealthSystem
    private HealthSystem Health { get; set; }

    private void Awake()
    {
        Health = gameObject.GetComponent<HealthSystem>();
    }
    //! Добавляет выносливость
    //! \param[in] stamina - Количество добавочной выносливости
    public void AddStamina(uint stamina)
    {
        Stamina += stamina;
        Health.GrowthMaxHealth(stamina * HealthStaminaRatio);
        Health.Healing(stamina * HealthStaminaRatio);
    }
    //! Уменьшает выносливость
    //! \param[in] stamina - Количество убавляемой выносливости
    public void ReductionStamina(uint stamina)
    {
        Stamina -= stamina;
        Health.ReductionMaxHealth(Stamina * HealthStaminaRatio);
    }
}
