﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
//! \brief Загручик сцены
//! \author Заплетин Дмитрий Евгеньевич
//! \version 1.0
//! \date 30.11.2019
public class SceneLoader : MonoBehaviour
{
    //! Scrollbar загрузки
    [SerializeField]
    private Scrollbar progressBar;
    //! Текст прогресса
    [SerializeField]
    private Text progressText;

    public void Load(string sceneName)
    {
        StartCoroutine(AsyncLoad(sceneName));
    }
    //! Асинхронная загрузка
    IEnumerator AsyncLoad(string sceneName)
    {
        AsyncOperation asyncOperation = SceneManager.LoadSceneAsync(sceneName, LoadSceneMode.Single);
        while(!asyncOperation.isDone)
        {
            var progress = asyncOperation.progress / 0.9f;
            progressBar.size = progress;
            progressText.text = $"Loading...{progress * 100:0}%";
            if(progress == 1f)
                gameObject.SetActive(false);
            yield return null;
        }
    }

}
