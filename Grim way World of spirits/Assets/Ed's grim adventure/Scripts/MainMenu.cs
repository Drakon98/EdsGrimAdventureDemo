﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
//! \brief Главное меню
//! \author Александр Игнатенко
//! \version 1.0
//! \date До 03.11.2019
public class MainMenu : MonoBehaviour
{
    //! Название первого уровня
    public string firstLevel;
    //! Название выбранного уровня
    public string levelSelect;
    //! Ссылка на кнопку загрузки
    public GameObject continueButton;
    //! Названия уровней
    public string[] levelNames;
    //! Кнопка начала игры
    [SerializeField]
    private Button play;

    void Start()
    {
        SelectedButton(play);
        if(PlayerPrefs.HasKey("Continue"))
        {
            continueButton.SetActive(true);
        }
        else
        {
            ResetProgress();
        }
    }
    //! Новая игра
    public void NewGame()
    {
        SceneManager.LoadScene(firstLevel);

        PlayerPrefs.SetInt("Continue", 0);
        PlayerPrefs.SetString("CurrentLevel", firstLevel);

        ResetProgress();
    }
    //! Загрузка игры
    public void Continue()
    {
        SceneManager.LoadScene(levelSelect);
    }
    //! Выход из игры
    public void QuitGame()
    {
        Application.Quit();
    }
    //! Восстановление прогресса
    public void ResetProgress()
    {
        for(int i = 0; i < levelNames.Length; i++)
        {
            PlayerPrefs.SetInt(levelNames[i] + "_unlocked", 0);
        }
    }
    //! Включение режима навигации для кнопки
    public void SelectedButton(Button button)
    {
        button.Select();
    }
}