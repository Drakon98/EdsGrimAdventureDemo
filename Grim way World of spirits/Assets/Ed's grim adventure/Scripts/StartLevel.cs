﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
//! \brief Загрузка начальной позиции игрока
//! \author Заплетин Дмитрий Евгеньевич
//! \version 1.0
//! \date 03.11.2019
public class StartLevel : MonoBehaviour
{
    private GameObject enemy;

    private void Awake()
    {
        var save = new Save();
        save.Read($"{Application.persistentDataPath}\\Save.json");
        var player = GameObject.Find("Player");
        if (player == null)
        {
            player = Resources.Load<GameObject>("Player");
            player = Instantiate(player);
            player.gameObject.name = "Player";
        }
        if (UIManager.IsNextLevel)
        {
            player.transform.position = gameObject.transform.position;
        }
        else
        {
            player.transform.position = save.Creatures[0].Position;
            var healthSystem = player.GetComponent<HealthSystem>();
            healthSystem.LoadHealth(save.Creatures[0].MaxHealth, save.Creatures[0].CurrentHealth);
            enemy = GameObject.Find("Enemy");
            List<GameObject> enemies = new List<GameObject>();
            for (int i = 0; i < enemy.transform.childCount; i++)
                enemies.Add(enemy.transform.GetChild(i).gameObject);
            for (int i = 0; i < enemies.Count; i++)
            {
                if (save.Creatures.Count - 1 > i)
                {
                    enemies[i].transform.position = save.Creatures[i + 1].Position;
                    enemies[i].GetComponent<HealthSystem>().LoadHealth(save.Creatures[i + 1].MaxHealth, save.Creatures[i + 1].CurrentHealth);
                }
                else
                {
                    Destroy(enemies[i]);
                }
            }
        }
    }
}
