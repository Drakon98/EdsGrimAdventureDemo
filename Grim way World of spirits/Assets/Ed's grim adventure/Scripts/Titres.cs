﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
//! \brief Проигрывает титры
//! \author Заплетин Дмитрий Евгеньевич
//! \version 1.0
//! \date 03.11.2019
//! \warning Скрипт должен быть на титрах
public class Titres : MonoBehaviour
{
    //! Цель смещения по оси Y 
    [SerializeField]
    private float target;
    //! Цель смещения по оси Y 
    public float Target { get => target; private set => target = value; }
    //! Скорость смещения к цели
    [SerializeField]
    private float speed;
    //! Скорость смещения к цели
    public float Speed { get => speed; private set => speed = value; }
    //! Время смещения к цели
    [SerializeField]
    private float timer;
    //! Время смещения к цели
    public float Timer { get => timer; private set { if (timer < 0f) timer = 0f; else timer = value; } }
    //! Ссылка на Transform титров
    private Transform Transform { get; set; }

    private void Start()
    {
        Transform = gameObject.transform;
    }

    void Update()
    {
        Timer -= Time.deltaTime;
        if (Input.GetKeyUp(KeyCode.Escape) || Timer == 0f)
        {
            SceneManager.LoadScene("MainMenu");
        }
        Transform.position = Vector2.MoveTowards(Transform.position, new Vector2(Transform.position.x, Target), Speed * Time.deltaTime);
    }
}
