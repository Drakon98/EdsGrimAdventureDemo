﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class SaveCreature
{
    public string Name;
    public Vector2 Position;
    public uint CurrentHealth;
    public uint MaxHealth;

    public SaveCreature()
    {

    }

    public SaveCreature(string name, Vector2 position, uint currentHealth, uint maxHealth)
    {
        Name = name;
        Position = position;
        CurrentHealth = currentHealth;
        MaxHealth = maxHealth;
    }


}
