﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//! \brief Простая атака
//! \author Заплетин Дмитрий Евгеньевич
//! \version 1.0
//! \date 03.11.2019
[RequireComponent(typeof(DamageObserver))]
public class SimpleAttack : MonoBehaviour, IDamageObservable
{
    //! Урон наносимый атакой
    [SerializeField]
    private uint damage;
    //! Урон наносимый атакой
    public uint Damage { get => damage; private set => damage = value; }
    //! Частота воспроизведения атаки
    [SerializeField]
    private float frequency;
    //! Частота воспроизведения атаки
    public float Frequency { get => frequency; private set => frequency = value; }
    //! Количество целей атаки
    [SerializeField]
    private uint countTarget;
    //! Количество целей атаки
    public uint CountTarget { get => countTarget; private set => countTarget = value; }
    //! Атака цели
    //! \param[in] target - Цель атаки
    public void Attack(GameObject target)
    {
        if(target != null)
            target.GetComponent<HealthSystem>().SetDamage(Damage);
    }
    //! Увеличение урона атаки
    //! \param[in] damage - Количество дополнительного урона
    public void AddDamage(uint damage) => Damage += damage;
    //! Уменьшение урона атаки
    //! \param[in] damage - Количество сниженного урона
    public void ReductionDamage(uint damage) => Damage -= damage;
    //! Увеличение частоты атаки (снижение скорости атаки)
    //! \param[in] frequency - Количество добавочной частоты атаки
    public void AddFrequency(float frequency) => Frequency += frequency;
    //! Снижение частоты атаки (увеличение скорости атаки)
    //! \param[in] frequency - Количество снижения частоты атаки
    public void ReductionFrequency(float frequency) => Frequency -= frequency;
}
