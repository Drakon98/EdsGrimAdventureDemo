﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//! \brief Способность восстановления здоровья
//! \author Заплетин Дмитрий Евгеньевич
//! \version 1.0
//! \date 03.11.2019
//! \warning Только для объектов обладающих HealthSystem
public class Healing : MonoBehaviour
{
    //! Количество восстанавливаемого здоровья
    [SerializeField]
    private uint hitPoints;
    //! Количество восстанавливаемого здоровья
    public uint HitPoints { get => hitPoints; private set => hitPoints = value; }
    //! Ссылка на HealthSystem
    private HealthSystem HealthSystem { get; set; }
    //! Эффект лечения
    [SerializeField]
    private ParticleSystem particleHealing;
    //! Эффект лечения
    public ParticleSystem ParticleHealing { get => particleHealing; private set => particleHealing = value; }

    private void Awake()
    {
        HealthSystem = gameObject.GetComponent<HealthSystem>();
    }

    void Update()
    {
        if (!UIManager.IsPause)
        {
            if (Input.GetKeyUp(KeyCode.Q))
            {
                HealthSystem.Healing(HitPoints);
                var particleHealing = Instantiate(ParticleHealing, gameObject.transform);
                particleHealing.Play();
                Destroy(particleHealing.gameObject, particleHealing.main.startLifetime.constant);
            }
        }
    }
}
