﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
//! \brief Загружает следующий уровень
//! \author Заплетин Дмитрий Евгеньевич
//! \version 1.0
//! \date 03.11.2019
public class LoadNewArea : MonoBehaviour
{
    //! Название следующей сцены \warning Пустая строка говорит об окончании игры
    [SerializeField]
    private string levelToLoad;
    //! Название следующей сцены
    public string LevelToLoad { get => levelToLoad; }
    //! Загрузка началась?
    public bool IsLoad { get; private set; }
    //! Ссылка на UIManager
    private UIManager Manager { get; set; }
    //! Ссылка на GameArbitrator
    private GameArbitrator Arbitrator { get; set; }

    /*private void Awake()
    {
        var menu = GameObject.Find("Menu");
        Manager = menu.GetComponent<UIManager>();
        Arbitrator = menu.GetComponent<GameArbitrator>();
    }*/

    private void OnTriggerEnter2D(Collider2D other)
    {
        var menu = GameObject.Find("Menu");
        Manager = menu.GetComponent<UIManager>();
        Arbitrator = menu.GetComponent<GameArbitrator>();
        if (other.gameObject.tag == "Player" && !IsLoad)
        {
            IsLoad = true;
            if (levelToLoad != "")
                Manager.LoadScene(levelToLoad, false);
            else
                Arbitrator.GameOver(true);

        }
    }
}
