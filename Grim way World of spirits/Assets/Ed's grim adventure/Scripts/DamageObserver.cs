﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
//! \brief Определяет нанесение атаки
//! \author Заплетин Дмитрий Евгеньевич
//! \version 2.0
//! \date 18.11.2019
public class DamageObserver : MonoBehaviour
{
    //! Теги атакуемых
    [SerializeField]
    private List<string> tagsObservable;
    //! Теги атакуемых
    public List<string> TagsObsevable { get => tagsObservable; private set => tagsObservable = value; }
    //! Атака определена?
    public bool IsAttack { get; private set; }
    //! Тип атаки
    public IDamageObservable TypeAttack { get; private set; }
    //! Обнаруженные цели атаки
    public List<GameObject> Targets { get; private set; } = new List<GameObject>();
    //! Таймер
    private float timer;
    //! Таймер
    public float Timer { get => timer; private set { if (timer < 0f) timer = 0f; else timer = value; } }

    private void Start()
    {
        TypeAttack = gameObject.GetComponent<IDamageObservable>();
        Timer = TypeAttack.Frequency;
    }

    private void Update()
    {
        if (IsAttack)
        {
            if (Timer != 0f)
            {
                Timer -= Time.deltaTime;
            }
            else if (Timer == 0f)
            {
                Timer = TypeAttack.Frequency;
                for (int i = 0; i < Targets.Count; i++)
                {
                    if (Targets[i] == null)
                        Targets.RemoveAt(i);
                }
                for (int i = 0; i < Targets.Count; i++)
                {
                    if (i > TypeAttack.CountTarget - 1)
                        break;
                    Attack(Targets[i]);
                }
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        foreach (var tag in TagsObsevable)
        {
            if (collision.gameObject.tag == tag)
            {
                IsAttack = true;
                if(!Targets.Exists(target => target == collision.gameObject))
                    Targets.Add(collision.gameObject);
            }
        }
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        foreach (var tag in TagsObsevable)
        {
            if (collision.gameObject.tag == tag)
            {
                IsAttack = true;
            }
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        foreach (var tag in TagsObsevable)
        {
            if (collision.gameObject.tag == tag)
            {
                if (Targets.Count == 0)
                    IsAttack = false;
            }
        }
    }
    //! Поражение цели
    //! \param[in] target - Цель атаки
    private void Attack(GameObject target)
    {
        TypeAttack.Attack(target.gameObject);
    }
    
}
