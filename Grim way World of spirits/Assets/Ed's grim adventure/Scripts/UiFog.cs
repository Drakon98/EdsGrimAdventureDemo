﻿using UnityEngine;
using UnityEngine.UI;

public class UiFog : MonoBehaviour
{
    public float Speed = 1;
    private Material _material;
    private Vector2 _baseOffset;

    void Start()
    {
        _material = GetComponent<Image>().material;
        _baseOffset = _material.mainTextureOffset;
    }

    // Update is called once per frame
    void Update()
    {
        _baseOffset.x += Time.unscaledDeltaTime * Speed;
        _material.SetTextureOffset("_MainTex", _baseOffset);
    }
}
