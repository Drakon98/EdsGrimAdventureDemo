﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//! \brief Блокирует игровые уровни до уничтожения всех врагов
//! \author Заплетин Дмитрий Евгеньевич
//! \version 1.0
//! \date 03.11.2019
//! \warning В сцене должен быть один корневой Enemy, который содержит всех врагов 
public class LevelLock : MonoBehaviour
{
    //! Коллайдер следующего уровня
    private BoxCollider2D NextLevel { get; set; }
    //! Ссылка на корневой объект Enemy, который содержит всех врагов
    private GameObject Enemy { get; set; }

    private void Awake()
    {
        NextLevel = gameObject.GetComponent<BoxCollider2D>();
        NextLevel.enabled = false;
        Enemy = GameObject.Find("Enemy");
        if (Enemy == null)
            NextLevel.enabled = true;
    }

    private void Update()
    {
        if (Enemy != null)
        {
            if (Enemy.transform.childCount == 0)
                NextLevel.enabled = true;
        }
    }
}
