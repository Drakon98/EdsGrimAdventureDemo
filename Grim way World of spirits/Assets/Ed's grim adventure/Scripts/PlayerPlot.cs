﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.Events;
//! \brief Проигрывает сценарий, состоящий из спрайтов
//! \author Заплетин Дмитрий Евгеньевич
//! \version 1.0
//! \date 03.11.2019
public class PlayerPlot : MonoBehaviour
{
    //! Хранит ссылку на проигрыватель
    [SerializeField]
    private Image playerImage;
    //! Хранит ссылку на проигрыватель
    public Image PlayerImage { get => playerImage; private set => playerImage = value; }
    //! Хранит список ссылок на спрайты сценария
    [SerializeField]
    private List<Sprite> sprites;
    //! Хранит список ссылок на спрайты сценария
    public List<Sprite> Sprites { get => sprites; private set => sprites = value; }
    //! Хранит список таймеров, отвечающих за переключение спрайтов сценария и событий связанных с ними
    [SerializeField]
    private List<float> timerSprite;
    //! Хранит список таймеров, отвечающих за переключение спрайтов сценария и событий связанных с ними
    public List<float> TimerSprite { get => timerSprite; private set => timerSprite = value; }
    //! Таймер
    private float timer;
    //! Таймер
    public float Timer { get => timer; private set { if (value < 0) timer = 0f; else timer = value; } }
    //! Таймер для показа панели с текстом
    private float timerPanelPlot = 5f;
    //! Таймер для показа панели с текстом
    public float TimerPanelPlot { get => timerPanelPlot; private set { if (value < 0) timerPanelPlot = 0f; else timerPanelPlot = value; } }
    //! Панель с текстом
    [SerializeField]
    private GameObject panelPlot;
    //! События сценария
    [SerializeField]
    private List<UnityEvent> events;
    //! События сценария
    public List<UnityEvent> Events { get => events; private set => events = value; }
    //! Текущий индекс сценария
    private int currentSpriteIndex;
    //! Сцена загружена?
    public bool IsLoadScene { get; private set; }
    //! Событие загрузки
    [SerializeField]
    private UnityEvent loading;
    //! Имя следующей сцены
    [SerializeField]
    private string nextNameScene;
    //! Имя следующей сцены
    public string NextNameScene { get => nextNameScene; private set => nextNameScene = value; }

    private void Start()
    {
        Timer = TimerSprite[0];
        Events[currentSpriteIndex].Invoke();
        panelPlot.SetActive(false);
    }

    private void Update()
    {
        InputNext();
        Next();
    }
    //! Переключение сцены клавишами
    private void InputNext()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            loading.Invoke();
            IsLoadScene = true;
        }
        else if (Input.GetMouseButtonDown(0) && !IsLoadScene)
        {
            if (Sprites.Count - 1 == currentSpriteIndex)
                loading.Invoke();
            else
                NextSprite();
        }
    }
    //! Переключение сцены таймером
    private void Next()
    {
        Timer -= Time.deltaTime;
        TimerPanelPlot -= Time.deltaTime;
        if (TimerPanelPlot == 0f)
            panelPlot.SetActive(true);
        if (Timer == 0f)
            NextSprite();
    }
    //! Переключение сцены
    private void NextSprite()
    {
        currentSpriteIndex++;
        if (Sprites.Count == currentSpriteIndex)
            SceneManager.LoadScene(NextNameScene);
        else
        {
            PlayerImage.sprite = Sprites[currentSpriteIndex];
            Timer = TimerSprite[currentSpriteIndex];
            Events[currentSpriteIndex].Invoke();
            panelPlot.SetActive(false);
            TimerPanelPlot = 5f;
        }
    }
}
