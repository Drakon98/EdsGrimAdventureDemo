﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
//! \brief Отвечает за обработку окончания игры
//! \author Заплетин Дмитрий Евгеньевич
//! \version 1.0
//! \date 03.11.2019
public class GameArbitrator : MonoBehaviour
{
    //! Название сцены при в случае выигрыша
    [SerializeField]
    private string winNameScene;
    //! Название сцены при в случае выигрыша
    public string WinNameScene { get => winNameScene; private set => winNameScene = value; }
    //! Ссылка на UIManager
    private static UIManager Manager { get; set; }

    private void Awake()
    {
        Manager = gameObject.GetComponent<UIManager>();
    }
    //! Вызывается при окончании игры
    //! \param[in] isResult - Результат игры
    public void GameOver(bool isResult)
    {
        if (isResult)
        {
            Manager.LoadScene(WinNameScene);
        }
        else
        {
            Manager.GameOver();
        }
    }
    //! Вызывается при отложенном окончании игры
    //! \param[in] isResult - Результат игры
    //! \param[in] time - Время загрузки
    //! \return IEnumerator
    public IEnumerator GameOver(bool isResult, float time)
    {
        yield return new WaitForSeconds(time);
        GameOver(isResult);
    }
}
