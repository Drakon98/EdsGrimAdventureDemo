﻿using System;
using System.IO;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class Save
{
    public string SceneName;
    public List<SaveCreature> Creatures = new List<SaveCreature>();

    public Save()
    {

    }

    public Save(string sceneName, List<SaveCreature> creatures)
    {
        SceneName = sceneName;
        Creatures = creatures;
    }

    public void Write(string path)
    {
        File.WriteAllText(path, JsonUtility.ToJson(this));
    }

    public void Read(string path)
    {
        var save = JsonUtility.FromJson<Save>(File.ReadAllText(path));
        SceneName = save.SceneName;
        Creatures = save.Creatures;
    }
}
