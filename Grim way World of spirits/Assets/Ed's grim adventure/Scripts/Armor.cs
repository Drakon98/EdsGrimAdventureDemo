﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//! \brief Доспехи выносливости
//! \author Заплетин Дмитрий Евгеньевич
//! \version 1.0
//! \date 03.11.2019
public class Armor : MonoBehaviour
{
    //! Защита
    [SerializeField]
    private uint protection;
    //! Защита
    public uint Protection { get => protection; private set => protection = value; }
    //! Принадлежность доспеха
    [SerializeField]
    private string nameOwner;
    //! Принадлежность доспеха
    public string NameOwner { get => nameOwner; set => nameOwner = value; }
    //! Ссылка на StaminaSystem
    private StaminaSystem Stamina { get; set; }

    private void Awake()
    {
        Stamina = GameObject.Find(NameOwner).GetComponent<StaminaSystem>();
        Stamina.AddStamina(protection);
    }


}
