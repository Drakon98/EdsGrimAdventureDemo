﻿using System;
using System.Collections.Generic;
using UnityEngine;
//! \brief Контракт для любого вида атаки
//! \author Заплетин Дмитрий Евгеньевич
//! \version 1.0
//! \date 03.11.2019
public interface IDamageObservable
{
    //! Урон наносимый атакой
    uint Damage { get; }
    //! Частота воспроизведения атаки
    float Frequency { get; }
    //! Количество целей атаки
    uint CountTarget { get; }
    //! Атака цели
    //! \param[in] target - Цель атаки
    void Attack(GameObject target);
    //! Увеличение урона атаки
    //! \param[in] damage - Количество дополнительного урона
    void AddDamage(uint damage);
    //! Уменьшение урона атаки
    //! \param[in] damage - Количество сниженного урона
    void ReductionDamage(uint damage);
    //! Увеличение частоты атаки (снижение скорости атаки)
    //! \param[in] frequency - Количество добавочной частоты атаки
    void AddFrequency(float frequency);
    //! Снижение частоты атаки (увеличение скорости атаки)
    //! \param[in] frequency - Количество снижения частоты атаки
    void ReductionFrequency(float frequency);

}
