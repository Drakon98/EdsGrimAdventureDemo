﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//! \brief Проигрывает музыку
//! \author Заплетин Дмитрий Евгеньевич
//! \version 1.0
//! \date 03.11.2019
public class MusicPlayer : MonoBehaviour
{
    //! Список клипов
    [SerializeField]
    private List<AudioClip> soundtracks;
    //! Список клипов
    public List<AudioClip> Soundtracks { get => soundtracks; private set => soundtracks = value; }
    //! Ссылка проигрыватель
    [SerializeField]
    private AudioSource source;
    //! Ссылка проигрыватель
    public AudioSource Source { get => source; private set => source = value; }
    //! Текущий индекс трека
    private int CurrentTrackIndex { get; set; }

    private void Start()
    {
        CurrentTrackIndex = 0;
    }

    private void Update()
    {
        if (!Source.isPlaying)
        {
            CurrentTrackIndex++;
            if (CurrentTrackIndex == Soundtracks.Count)
                CurrentTrackIndex = 0;
            Source.clip = Soundtracks[CurrentTrackIndex];
            Source.Play();
        }
    }
}
