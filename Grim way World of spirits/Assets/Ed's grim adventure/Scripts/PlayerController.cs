﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//! \brief Контроллер игрока
//! \author Заплетин Дмитрий Евгеньевич
//! \version 2.0
//! \date 17.11.2019
public class PlayerController : BaseController
{
    //! Ссылка на GameArbitrator игрока
    private GameArbitrator Arbitrator { get; set; }
    //! Ссылка на триггерный BoxCollider
    [SerializeField]
    private BoxCollider2D boxCollider;

    private void Awake()
    {
        Arbitrator = GameObject.Find("Menu").GetComponent<GameArbitrator>();
    }

    protected override void Start()
    {
        base.Start();
        Animator.SetFloat("MoveY", -1);
        TargetTag = "Enemy";
    }

    void Update()
    {
        if (!UIManager.IsPause)
        {
            Move();
            MoveUpdate();
            InputAttack();
        }
    }
    //! Обновление движения
    private void MoveUpdate()
    {
        Animator.SetBool("PlayerMoving", IsMoving);
        if (Mathf.Abs(Horizontal) == 1f || Mathf.Abs(Vertical) == 1f)
        {
            IsMoving = true;
            MoveAnimate();
        }
        else
            IsMoving = false;
    }
    //! Определение направления движения
    protected override void Move_DefinitionTarget()
    {
        Horizontal = Input.GetAxisRaw("Horizontal");
        Vertical = Input.GetAxisRaw("Vertical");
    }
    //! Ввод атаки игроком
    private void InputAttack()
    {
        if (Input.GetMouseButtonDown(0))
        {
            ChangeTarget();
            if (IsReachableTarget)
            {
                Animator.SetFloat("MoveX", Mathf.Round(AttackVector.x));
                Animator.SetFloat("MoveY", Mathf.Round(AttackVector.y));
            }
        }
    }
    //! Смена атакуемой цели
    private void ChangeTarget()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit2D hit = Physics2D.Raycast(ray.origin, ray.direction);
        if (hit.collider != null)
        {
            if (hit.collider.gameObject.tag == TargetTag)
            {
                DefinitionTarget(hit.collider);
                FindingTarget(hit.collider.gameObject);
                Attack(true);
            }
        }
    }
    //! Поиск доступности цели
    private void FindingTarget(GameObject target)
    {
        var colliders = Physics2D.OverlapBoxAll(new Vector2(transform.position.x, transform.position.y), boxCollider.size, 0f);
        foreach (var collider in colliders)
        {
            if(collider.gameObject == target)
            {
                IsReachableTarget = true;
            }
        }
    }
    //! Проигрывание смерти игрока
    //! \param[in] timerDied - Время смерти
    public override void Died(float timeDied)
    {
        base.Died(timeDied);
        gameObject.GetComponent<Healing>().enabled = false;
        StartCoroutine(Arbitrator.GameOver(false, timeDied));
    }

}
