﻿using UnityEngine;
using System.IO;
using System;

[Serializable]
public class Settings //: MonoBehaviour
{
    public float Sound = 1f;
    public float Music = 1f;

    public void Read(string path)
    {
        var settings = JsonUtility.FromJson<Settings>(File.ReadAllText(path));
        Sound = settings.Sound;
        Music = settings.Music;
    }

    public void Write(string path)
    {
        File.WriteAllText(path, JsonUtility.ToJson(this));
    }
}
