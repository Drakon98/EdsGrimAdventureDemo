﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.EventSystems;
using UnityEngine.Events;
//! \brief Класс отвечает за загрузку игровых сцен и игровую паузу
//! \author Заплетин Дмитрий Евгеньевич
//! \version 1.0
//! \date 03.11.2019
//! \warning Загрузка сцен ведётся только через этот класс
public class UIManager : MonoBehaviour
{
    //! Хранит ссылку на экран паузы
    [SerializeField]
    private GameObject pauseScreen;
    //! Хранит ссылку на экран паузы
    [SerializeField]
    private GameObject optionsScreen;
    //! Меню смерти
    [SerializeField]
    private GameObject menuGameOver;
    //! Меню смерти
    public GameObject MenuGameOver { get => menuGameOver; private set => menuGameOver = value; }
    //! Хранит ссылку на игрока
    //private GameObject Player { get; set; }
    //! Кнопка продолжения игры
    [SerializeField]
    private GameObject resume;
    //! Кнопка загрузки в меню смерти
    [SerializeField]
    private GameObject load;
    //! Ссылка на систему событий
    [SerializeField]
    private EventSystem eventSystem;
    //! Событие загрузки
    [SerializeField]
    private UnityEvent loading;
    [SerializeField]
    private SceneLoader sceneLoader;

    public static bool IsPause { get; set; }
    public static bool IsNextLevel { get; set; } = true;

    /*private void Awake()
    {
        Player = GameObject.Find("Player");
    }*/

    void Update()
    {
        Pause();
    }
    //! Включает и отключает паузу
    private void Pause()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (!optionsScreen.activeSelf)
            {
                if (pauseScreen.activeInHierarchy)
                {
                    IsPause = false;
                    pauseScreen.SetActive(false);
                    eventSystem.SetSelectedGameObject(pauseScreen);
                    Time.timeScale = 1f;
                }
                else
                {
                    IsPause = true;
                    pauseScreen.SetActive(true);
                    eventSystem.SetSelectedGameObject(resume);
                    Time.timeScale = 0f;
                }
            }
        }
    }
    //! Для продолжения игры
    public void Resume()
    {
        IsPause = false;
        eventSystem.SetSelectedGameObject(pauseScreen);
        pauseScreen.SetActive(false);
        Time.timeScale = 1f;
    }
    //! Для загрузки игровых сцен
    //! \param[in] nameScene - название загружаемой сцены
    public void LoadScene(string nameScene)
    {
        ClearGameObjects();
        IsNextLevel = true;
        Load(nameScene);
        Time.timeScale = 1f;
    }

    //! Для загрузки игровых сцен
    public void LoadSceneAuto()
    {
        ClearGameObjects();
        IsNextLevel = false;
        var save = new Save();
        save.Read($"{Application.persistentDataPath}\\Save.json");
        Load(save.SceneName);
        Time.timeScale = 1f;
    }

    private void Load(string nameScene)
    {
        IsPause = false;
        loading.Invoke();//SceneManager.LoadScene(nameScene);
        sceneLoader.Load(nameScene);
    }
    //! Для загрузки неигровых сцен 
    //! \param[in] nameScene - название загружаемой сцены
    //! \param[in] isClear - указывает нужна ли очистка от неразрушаемых объектов
    public void LoadScene(string nameScene, bool isClear)
    {
        if (isClear)
            ClearGameObjects();
        IsNextLevel = true;
        IsPause = false;
        Load(nameScene);
        Time.timeScale = 1f;
    }
    //! Очистка неразрушаемых объектов
    public void ClearGameObjects()
    {
        Destroy(GameObject.Find("Player"));
    }
    //! Включение режима навигации для кнопки
    public void SelectedButton(Button button)
    {
        button.Select();
    }
    //! Окончание игры
    public void GameOver()
    {
        MenuGameOver.SetActive(true);
        eventSystem.SetSelectedGameObject(load);
    }
}
