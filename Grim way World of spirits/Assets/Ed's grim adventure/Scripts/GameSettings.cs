﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameSettings : MonoBehaviour
{
    public Slider musicSlider;
    public Text valueCount;
    public AudioSource music;

    private void Start()
    {   
        if (valueCount != null)
        valueCount.text = (musicSlider.value * 100).ToString();
    }

    public void ChangeVolume()
    {
        valueCount.text = (musicSlider.value * 100).ToString();
        music.volume = musicSlider.value;
    }

    public void LoadSettings(float value)
    {
        if (musicSlider != null)
            musicSlider.value = value;
        music.volume = value;
        if (valueCount != null)
            valueCount.text = $"{value * 100:00}";
    }
}
