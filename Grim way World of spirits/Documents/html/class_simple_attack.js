var class_simple_attack =
[
    [ "AddDamage", "class_simple_attack.html#ae4c58443bbd6fc6e08b4faccc8409f60", null ],
    [ "AddFrequency", "class_simple_attack.html#a46ab44f70a177feb827a9f99cab8907a", null ],
    [ "Attack", "class_simple_attack.html#a8d49dd57b139d2509aa90fd54ac9fc7c", null ],
    [ "ReductionDamage", "class_simple_attack.html#af71ee3ab114c368e683c9b51e8e5dcec", null ],
    [ "ReductionFrequency", "class_simple_attack.html#a7683d56c860d0f9b43a8cf1b799c139f", null ],
    [ "countTarget", "class_simple_attack.html#a48580c899c6fb471d3110b3a043e469b", null ],
    [ "damage", "class_simple_attack.html#a58dd53d8ae428308b9b4bdb4e7268a78", null ],
    [ "frequency", "class_simple_attack.html#a61c68bad751803b3382d6eae0aad5201", null ],
    [ "CountTarget", "class_simple_attack.html#a60ba542482dac8f8f9f7312cc35d2be4", null ],
    [ "Damage", "class_simple_attack.html#a18276d227f354f3f0568372051cd392d", null ],
    [ "Frequency", "class_simple_attack.html#a2f671f712934ebe1ecc7885c54f69a5f", null ]
];