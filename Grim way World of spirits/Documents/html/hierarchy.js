var hierarchy =
[
    [ "IDamageObservable", "interface_i_damage_observable.html", [
      [ "SimpleAttack", "class_simple_attack.html", null ]
    ] ],
    [ "MonoBehaviour", null, [
      [ "Armor", "class_armor.html", null ],
      [ "BaseController", "class_base_controller.html", [
        [ "EnemyController", "class_enemy_controller.html", null ],
        [ "PlayerController", "class_player_controller.html", null ]
      ] ],
      [ "DamageObserver", "class_damage_observer.html", null ],
      [ "DontDestoryObject", "class_dont_destory_object.html", null ],
      [ "GameArbitrator", "class_game_arbitrator.html", null ],
      [ "GameSettings", "class_game_settings.html", null ],
      [ "Healing", "class_healing.html", null ],
      [ "HealthSystem", "class_health_system.html", null ],
      [ "LevelLock", "class_level_lock.html", null ],
      [ "LoadNewArea", "class_load_new_area.html", null ],
      [ "MainMenu", "class_main_menu.html", null ],
      [ "MusicPlayer", "class_music_player.html", null ],
      [ "PlayerPlot", "class_player_plot.html", null ],
      [ "SceneLoader", "class_scene_loader.html", null ],
      [ "SettingsObserver", "class_settings_observer.html", null ],
      [ "SimpleAttack", "class_simple_attack.html", null ],
      [ "StaminaSystem", "class_stamina_system.html", null ],
      [ "StartLevel", "class_start_level.html", null ],
      [ "Titres", "class_titres.html", null ],
      [ "UIManager", "class_u_i_manager.html", null ]
    ] ],
    [ "Settings", "class_settings.html", null ]
];