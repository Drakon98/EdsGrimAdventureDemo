var class_main_menu =
[
    [ "Continue", "class_main_menu.html#aa9db4c20ba13ea077ab2fca568e1b0c9", null ],
    [ "NewGame", "class_main_menu.html#a0b9845990e48b948575d2bf6f4dad24d", null ],
    [ "QuitGame", "class_main_menu.html#a485db7cf60c0b93ecc87b9273bcce78b", null ],
    [ "ResetProgress", "class_main_menu.html#ae1838b0da295cf05d75838a172252869", null ],
    [ "SelectedButton", "class_main_menu.html#a68865913517b7651d8e3f8aa8574b3e6", null ],
    [ "Start", "class_main_menu.html#ad629921d8cbec1c1bf97e6bc6ad65902", null ],
    [ "continueButton", "class_main_menu.html#a3c956a34110a6d6da4f7909f837d0d05", null ],
    [ "firstLevel", "class_main_menu.html#af12f2a094ecebaec24f3fd032cebd3f3", null ],
    [ "levelNames", "class_main_menu.html#a87d2d960cfc0b5ec5d99073f001b1983", null ],
    [ "levelSelect", "class_main_menu.html#ad366c7d657ff5c397b2273b69fbe92f0", null ],
    [ "play", "class_main_menu.html#a4f10903beff10f430c2eabc4c38174b4", null ]
];