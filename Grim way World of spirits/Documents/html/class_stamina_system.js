var class_stamina_system =
[
    [ "AddStamina", "class_stamina_system.html#ac174d6b689cf251afa89b56f5745ef76", null ],
    [ "Awake", "class_stamina_system.html#a27a61e5b42262546e58121a32aacbaf7", null ],
    [ "ReductionStamina", "class_stamina_system.html#ad5dc4dbeb3fd217226703fcb9a79cf2e", null ],
    [ "healthStaminaRatio", "class_stamina_system.html#a41d2808989816efdea2f398916805ec5", null ],
    [ "stamina", "class_stamina_system.html#a32574034eef58212b0f5fc3ca15d2b2d", null ],
    [ "Health", "class_stamina_system.html#a21d8e5f0702547d89ddfe80ee3acee6f", null ],
    [ "HealthStaminaRatio", "class_stamina_system.html#ae80243430db4c9d9120eb424c2a6babc", null ],
    [ "Stamina", "class_stamina_system.html#a5d917ec8076a204ea7331082d4457dd5", null ]
];