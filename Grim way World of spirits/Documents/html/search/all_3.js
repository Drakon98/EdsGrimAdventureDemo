var searchData=
[
  ['damage',['Damage',['../interface_i_damage_observable.html#a55980aeda1b2c3b289bb36aaba877af1',1,'IDamageObservable.Damage()'],['../class_simple_attack.html#a18276d227f354f3f0568372051cd392d',1,'SimpleAttack.Damage()'],['../class_simple_attack.html#a58dd53d8ae428308b9b4bdb4e7268a78',1,'SimpleAttack.damage()']]],
  ['damageobserver',['DamageObserver',['../class_damage_observer.html',1,'']]],
  ['definitiontarget',['DefinitionTarget',['../class_base_controller.html#a94e24ceb7e7c99aa2f8c287be6612cd3',1,'BaseController']]],
  ['died',['Died',['../class_base_controller.html#ad02badbba086ef6e6664f0fb8246269d',1,'BaseController.Died()'],['../class_enemy_controller.html#a039ce9f772a5e69ff7d01f519843d11a',1,'EnemyController.Died()'],['../class_player_controller.html#a7849775a93670452e0b42697c187e54f',1,'PlayerController.Died()']]],
  ['dontdestoryobject',['DontDestoryObject',['../class_dont_destory_object.html',1,'']]]
];
