var searchData=
[
  ['reductiondamage',['ReductionDamage',['../interface_i_damage_observable.html#a5c73f0d6ea7ad475508f1030070a1c24',1,'IDamageObservable.ReductionDamage()'],['../class_simple_attack.html#af71ee3ab114c368e683c9b51e8e5dcec',1,'SimpleAttack.ReductionDamage()']]],
  ['reductionfrequency',['ReductionFrequency',['../interface_i_damage_observable.html#a4e9d22b146ca50d2ed90f067905e5f1e',1,'IDamageObservable.ReductionFrequency()'],['../class_simple_attack.html#a7683d56c860d0f9b43a8cf1b799c139f',1,'SimpleAttack.ReductionFrequency()']]],
  ['reductionmaxhealth',['ReductionMaxHealth',['../class_health_system.html#ab0d21f474c6b427c52d1768d1d71997d',1,'HealthSystem']]],
  ['reductionstamina',['ReductionStamina',['../class_stamina_system.html#ad5dc4dbeb3fd217226703fcb9a79cf2e',1,'StaminaSystem']]],
  ['resetprogress',['ResetProgress',['../class_main_menu.html#ae1838b0da295cf05d75838a172252869',1,'MainMenu']]],
  ['resume',['Resume',['../class_u_i_manager.html#a14ba377aeafd27e782d3f25107b39e1a',1,'UIManager']]]
];
