var searchData=
[
  ['idamageobservable',['IDamageObservable',['../interface_i_damage_observable.html',1,'']]],
  ['inputattack',['InputAttack',['../class_player_controller.html#a8d31b104a45e106d3586bbdc7c6e294b',1,'PlayerController']]],
  ['inputnext',['InputNext',['../class_player_plot.html#a12d441bd9c621fa16a6e1dfb5bb57b26',1,'PlayerPlot']]],
  ['isattack',['IsAttack',['../class_base_controller.html#a892d64df988f54348a032eda86efb33b',1,'BaseController.IsAttack()'],['../class_damage_observer.html#a807470075560c76f5d62e51e03984f15',1,'DamageObserver.IsAttack()']]],
  ['isdied',['IsDied',['../class_base_controller.html#a32fc48cdb31ac9bc817d7130de72a149',1,'BaseController.IsDied()'],['../class_health_system.html#ae232a4a8b28c27808e51f43cdcbdf512',1,'HealthSystem.IsDied()']]],
  ['isload',['IsLoad',['../class_load_new_area.html#a91a9377a0737fcc276c7b034eaf5f753',1,'LoadNewArea']]],
  ['isloadscene',['IsLoadScene',['../class_player_plot.html#aad2eb86078f4f70ec5ed12cef58430dc',1,'PlayerPlot']]],
  ['ismoving',['IsMoving',['../class_base_controller.html#a4fd18eacba51fb8ed9ad41c328e2453e',1,'BaseController']]],
  ['isreachabletarget',['IsReachableTarget',['../class_base_controller.html#a09ef62a64dcb3c487c61cb7e4c5c5b30',1,'BaseController']]]
];
