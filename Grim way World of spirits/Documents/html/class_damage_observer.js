var class_damage_observer =
[
    [ "Attack", "class_damage_observer.html#abd72833dc03e4796db3cdd3c88d6add2", null ],
    [ "OnTriggerEnter2D", "class_damage_observer.html#ae271462f434c13a4db9e7ed1e3a782e8", null ],
    [ "OnTriggerExit2D", "class_damage_observer.html#aa619c7761d4f8ae8af852ad5edeb998a", null ],
    [ "OnTriggerStay2D", "class_damage_observer.html#a8ef045eb65b4c16835d33c335256b9e5", null ],
    [ "Start", "class_damage_observer.html#acd57c07480e993bfd9d8b68034832052", null ],
    [ "Update", "class_damage_observer.html#a3acee8fd251017cc4fc5f9f3824ca04c", null ],
    [ "tagsObservable", "class_damage_observer.html#a26f1bb314f2e48aeb58004c5420655b8", null ],
    [ "timer", "class_damage_observer.html#ad34ac2e40d9a49903b5f96c874d8ade7", null ],
    [ "IsAttack", "class_damage_observer.html#a807470075560c76f5d62e51e03984f15", null ],
    [ "TagsObsevable", "class_damage_observer.html#a1be6bee983436667aeb5dcd599751890", null ],
    [ "Targets", "class_damage_observer.html#a2afaa7e7f377dacba78330b3dff3248e", null ],
    [ "Timer", "class_damage_observer.html#a7095f1fb775f6a0239c005c0fd221629", null ],
    [ "TypeAttack", "class_damage_observer.html#a5dfbb16d656fbd7dac1a0fdbb944d2d1", null ]
];