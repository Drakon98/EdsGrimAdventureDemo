var class_health_system =
[
    [ "GrowthMaxHealth", "class_health_system.html#af223da978d717fe7e2f8bbb06cc49d31", null ],
    [ "Healing", "class_health_system.html#a2471ebeb4350914648a22f522574fa8e", null ],
    [ "LoadHitPoints", "class_health_system.html#ae77eae7f2617bd1e91536dcbd6b34bc7", null ],
    [ "PlayDied", "class_health_system.html#ab6ce6a138fd733161bc8ac2a91b608d1", null ],
    [ "ReductionMaxHealth", "class_health_system.html#ab0d21f474c6b427c52d1768d1d71997d", null ],
    [ "SetDamage", "class_health_system.html#ae4e313caaad7f56c7f37c159428e5935", null ],
    [ "Start", "class_health_system.html#a4b1bc3e4699d109081b60320b4483993", null ],
    [ "VerificationHealth", "class_health_system.html#adc83cd787161ea12e203184ad058fd75", null ],
    [ "currentHealth", "class_health_system.html#add0af3c35f2bc00677a71134920d25f3", null ],
    [ "eventIsDied", "class_health_system.html#a83fbdc3fc34d9e653853113e80e6b5f1", null ],
    [ "healthText", "class_health_system.html#a03ef520a6ebc96206fd318c548166929", null ],
    [ "hitPoints", "class_health_system.html#ae14255948112b84068d3cc57d1190567", null ],
    [ "IsDied", "class_health_system.html#ae232a4a8b28c27808e51f43cdcbdf512", null ],
    [ "maxHealth", "class_health_system.html#aeae6ac28f5adb83e5e66e7704d5012ec", null ],
    [ "CurrentHealth", "class_health_system.html#a06f24ca03c3bc392885a08c19800549d", null ],
    [ "EventIsDied", "class_health_system.html#abf93143d64dae09e98c2c621e1514473", null ],
    [ "HealthText", "class_health_system.html#adc26ff706578e16cf494d0d2261767d4", null ],
    [ "HitPoints", "class_health_system.html#a024923c32a558ac2f8f4e9f870598521", null ],
    [ "MaxHealth", "class_health_system.html#a36db3e6b1e271a7f36e4d994778696b4", null ]
];