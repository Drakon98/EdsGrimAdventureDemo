var class_settings_observer =
[
    [ "ChangeMusic", "class_settings_observer.html#a1257eea4dc5bf3114c0d7df223e75a83", null ],
    [ "ChangeSound", "class_settings_observer.html#ad503802e0d1ce01b7a856479a5b14e5e", null ],
    [ "Start", "class_settings_observer.html#abce5320d5149cf14380229954f1e31a2", null ],
    [ "_musicSettings", "class_settings_observer.html#a5d98382b4205c319234bd84b1b981036", null ],
    [ "_path", "class_settings_observer.html#a2ebe2366ab958f690950a8eb8256c6e6", null ],
    [ "_settings", "class_settings_observer.html#aadcceac21da67d2a7aa2e3b400288d08", null ],
    [ "_soundSettings", "class_settings_observer.html#ae448c2ac4a3e9e891d3ce76d4817fc24", null ]
];