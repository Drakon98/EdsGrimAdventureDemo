var interface_i_damage_observable =
[
    [ "AddDamage", "interface_i_damage_observable.html#a6bc9dd0335494bfe9b3d3dabfb10c6f1", null ],
    [ "AddFrequency", "interface_i_damage_observable.html#ac75aa25eb4987122c201a9fac368e0bf", null ],
    [ "Attack", "interface_i_damage_observable.html#aecbbf1fdb0b47d54bb9788a2dc0f7fda", null ],
    [ "ReductionDamage", "interface_i_damage_observable.html#a5c73f0d6ea7ad475508f1030070a1c24", null ],
    [ "ReductionFrequency", "interface_i_damage_observable.html#a4e9d22b146ca50d2ed90f067905e5f1e", null ],
    [ "CountTarget", "interface_i_damage_observable.html#a9728227bcb29992e3dd09ef63b6534e3", null ],
    [ "Damage", "interface_i_damage_observable.html#a55980aeda1b2c3b289bb36aaba877af1", null ],
    [ "Frequency", "interface_i_damage_observable.html#a088549525065a7005d7de535a4e197d5", null ]
];